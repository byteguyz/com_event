<?php
	session_start();
	if (!isset($_SESSION['isAdmin'])) {
		header('Location: http://byteguyz.org');
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
		<?php require 'includes/connect.inc'; ?>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<div class="blogText">
					<h3>Member Donations</h3>
					<?php
						require 'includes/connect.inc';
						$sql = "SELECT u.username, sum(donation) AS total FROM Users AS u, Donations as d WHERE d.usersID = u.usersID GROUP BY u.username";
						$data = $db->query($sql);
						if ($data->num_rows > 0) {
							while($row = $data->fetch_assoc()) {
									echo "<p>" . $row['username'] . ": " . $row['total'] . "</p>";
							}
						}
					?>
				</div>
			</article>
			
            <article class="column2">
				
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
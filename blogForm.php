<?php
	session_start();
	require 'includes/validation.inc';
	require 'includes/connect.inc';
	
	if (!isset($_SESSION['isAdmin'])) {
		header('Location: http://byteguyz.org');
	}
	
	//if the admin submits an event, run through validation and submit to database
	if(isset($_POST['login'])) {
		$errMsg = '';
		$target_dir = "images/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$target_dir_addr = "/images/";
		$target_file_addr = $target_dir_addr . basename($_FILES["fileToUpload"]["name"]);
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		$blogName = validate($_POST['blogName']);
		$blogInfo = validate($_POST['blogInfo']);
		$blogInfo = nl2br($blogInfo);
		$blogSummary = validate($_POST['blogSummary']);
		$blogSummary = nl2br($blogSummary);
		$blogDate = date('Y-m-d');
		$usersID = $_SESSION['usersID'];
		echo $imageFileType;
		echo $target_file_addr;
		
		if($blogName == '') {
			$errMsg .= 'You must enter your blog title<br>';
		}
		elseif (!preg_match("/^[a-zA-Z ]{5,40}$/", $blogName)) {
			$errMsg .= 'Your blog name must contain only letters and be between 5 to 40 characters in length<br>';
		}
		if($blogInfo == '') {
			$errMsg .= 'You must enter your information on the blog<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{100,}/", $blogInfo)) {
			$errMsg .= 'Your blog information must be greater than 100 characters, and only include common punctuation<br>';
		}
		if($blogSummary == '') {
			$errMsg .= 'You must enter your summary on the blog<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{100,300}/", $blogSummary)) {
			$errMsg .= 'Your summary must be between 100 to 200 characters, and only include common punctuation<br>';
		}
		
		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 3000000) {
			$errMsg .= 'Sorry, your file is too large. Your file should be under 60kb';

		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$errMsg .= 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';

		}
		
		//if the form input passes validation, submit to database
		if (!$errMsg) {
			require 'includes/connect.inc';
			$stmt = $db->prepare("INSERT INTO Blogs (usersID, blogName, blogDate, blogInfo, blogSummary, imageHeader) VALUES (?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('dsssss', $usersID, $blogName, $blogDate, $blogInfo, $blogSummary, $target_file_addr);
			$stmt->execute();
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
			header('Location: http://byteguyz.org/blog.php');
		}
	}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>
	
	<body>
		<?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<div class="formStyle">
					<h2>Create a Blog Entry</h2>
					<form action="blogForm.php" method="POST" id="blogForm" enctype="multipart/form-data">
						<div class="requiredField">
							<input name="blogName" id="blogName" type="text" placeholder="Title" value="<?php if(isset($_POST['login'])){ echo $_POST['blogName'];}?>" required/>
						</div>
						<div class="requiredField">
							<textarea rows="4" cols="50" name="blogInfo" id="blogInfo" form="blogForm" placeholder="Blog Information" required><?php if(isset($_POST['login'])){ echo $_POST['blogInfo'];}?></textarea>			
						</div>
						<div class="requiredField">
							<textarea rows="4" cols="50" name="blogSummary" id="blogSummary" form="blogForm" placeholder="Blog Summary - between 100 to 150 characters"  required><?php if(isset($_POST['login'])){ echo $_POST['blogSummary'];}?></textarea>			
						</div>
						<div class="requiredField">
							<label for="fileToUpload">Upload image:</label>
							<input type="file" name="fileToUpload" id="fileToUpload">
						</div>
						<input type="submit" name="login" value="Create"/>
						<span id="errorField" class="errorField">
							<?php 
								echo $errMsg; 
							?>
						</span>
					</form>
				</div>
			</article>
		</section>
		<?php require 'includes/footer.inc'; ?>
	</body>
</html>
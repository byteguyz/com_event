<?php
	session_start();
	require 'includes/connect.inc';
	require 'includes/eventsRegister.inc';
	$eventID = $_GET['eventID'];
	require 'includes/memberEventInformation.inc';
	
	if (!isset($_SESSION['isAdmin'])) {
		header('Location: http://byteguyz.org');
	}
	//if the admin submits the form, submit event to the database to display to all users and delete member submitted event
	if(isset($_POST['login'])) {
		registerEvent($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary);	
		$statement = $db->prepare("DELETE FROM MemberEvents WHERE eventID=?");
		$statement->bind_param('d', $eventID);
		$statement->execute();
		header('Location: http://byteguyz.org/eventsApproval.php');
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
        <section id="text_columns">
            <article class="column1">
				<?php
				echo"<div class='blogPicture' style='position:relative; overflow: hidden;'>";
					if ($imageHeader == ''){
					echo"<img src='/images/499055836.jpg' style='width:100%;' />";
					}
					else{
						echo"<img src='". $imageHeader . "' style='width:100%; position: absolute; top:-9999px; bottom:-9999px; left:-9999px; right:-9999px; margin: auto;' />";
					}
				echo"</div>";
				?>
				<?php 
					//display the information of the member submitted event to the admin
					echo "<div class='blogText'>";
						echo "<h3>" . $eventName . "</h3>";
						echo "<p>" . $eventDate . "</p>";
						echo "<p>" . $eventLocation . "</p>";
						echo "<p>" . $eventCapacity . "</p>";
						echo "<p>" . $eventMemberCost . "</p>";
						echo "<p>" . $eventInfo . "</p>";
					echo "</div>";
				?>
				<div class="formStyle">
					<form action="http://byteguyz.org/eventInfoApproval.php?eventID=<?php echo $eventID ?>" method="POST" id="eventForm">
						<input type="submit" name="login" value="Approve Event"/>
					</form>
				</div>
			</article>
			
            <article class="column2">
				
			</article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>

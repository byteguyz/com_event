<script>
	/* When the user clicks on the button,
	toggle between hiding and showing the dropdown content */
	function myFunction1() {
		document.getElementById("myDropdown1").classList.toggle("show");
	}
	
	function myFunction2() {
		document.getElementById("myDropdown2").classList.toggle("show");
	}
	
	function myFunction3() {
		document.getElementById("myDropdown3").classList.toggle("show");
	}
	
	function myFunction4() {
		document.getElementById("myDropdown4").classList.toggle("show");
	}

	// Close the dropdown if the user clicks outside of it
	window.onclick = function(e) {
	  if (!e.target.matches('.dropbtn')) {

		var dropdowns = document.getElementsByClassName("dropdown-content");
		for (var d = 0; d < dropdowns.length; d++) {
		  var openDropdown = dropdowns[d];
		  if (openDropdown.classList.contains('show')) {
			openDropdown.classList.remove('show');
		  }
		}
	  }
	}
</script>

<header>
	<div class="toggleMobile">
		<span class="menu1"></span>
		<span class="menu2"></span>
		<span class="menu3"></span>
	</div>
	<div id="mobileMenu">
		<ul>
			<?php
				echo '<li class="events">';
					echo '<a href="javascript:void(0)" class="dropbtn" onclick="myFunction4()">Events</a>';
					echo '<div class="dropdown-content" id="myDropdown4">';
						echo '<a href="events.php">View Events</a>';
						echo '<a href="calendar.php">Calendar</a>';
                        if (isset($_SESSION['isAdmin'])) {
				              echo '<a href="forum.php">Forum</a>';
							}
					echo '</div>';
				echo '</li>';
			?>
			<li class="about"><a href="about.php">About</a></li>
			<li class="blog"><a href="blog.php">Blog</a></li>
			<?php
				if (!isset($_SESSION['isAdmin']) && !isset($_SESSION['isUser'])) {
					echo '<li class="login"><a href="login.php">Login</a></li>';
				}
				else {
					echo '<li class="logout">';
						echo '<a href="javascript:void(0)" class="dropbtn" onclick="myFunction1()">Profile</a>';
						echo '<div class="dropdown-content" id="myDropdown1">';
							echo '<a href="accountEdit.php">Edit Account</a>';
							if (isset($_SESSION['isAdmin'])) {
								echo '<a href="userDonations.php">Donations</a>';
							}
							echo '<a href="logout.php">Logout</a>';
						echo '</div>';
					echo '</li>';
				}
			?>
		</ul>
	</div>
	
	<div id="heading">
		<h1><a href="index.php">Community Events</a></h1>
	</div>
	<nav>
		<ul>
			<?php
				echo '<li class="events">';
					echo '<a href="javascript:void(0)" class="dropbtn" onclick="myFunction3()">Events</a>';
					echo '<div class="dropdown-content" id="myDropdown3">';
						echo '<a href="events.php">View Events</a>';
						echo '<a href="calendar.php">Calendar</a>';
                        if (isset($_SESSION['isAdmin'])) {
								echo '<a href="forum.php">Forum</a>';
							}
					echo '</div>';
				echo '</li>';
			?>
			<li class="about"><a href="about.php">About</a></li>
			<li class="blog"><a href="blog.php">Blog</a></li>
			<?php
				if (!isset($_SESSION['isAdmin']) && !isset($_SESSION['isUser'])) {
					echo '<li class="login"><a href="login.php">Login</a></li>';
				}
				else {
					echo '<li class="logout">';
						echo '<a href="javascript:void(0)" class="dropbtn" onclick="myFunction2()">Profile</a>';
						echo '<div class="dropdown-content" id="myDropdown2">';
							echo '<a href="accountEdit.php">Edit Account</a>';
							if (isset($_SESSION['isAdmin'])) {
								echo '<a href="userDonations.php">Donations</a>';
							}
							echo '<a href="logout.php">Logout</a>';
						echo '</div>';
					echo '</li>';
				}
			?>
		</ul>
	</nav>
</header>


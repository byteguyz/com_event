<?php 
	//select and bind all data from the users table to local variables
	$statement = $db->prepare("SELECT * FROM Users WHERE usersID = ?");
	$statement->bind_param('d', $usersID);	
	$statement->execute();
	$statement->store_result();
	$statement->bind_result($usersID, $username, $firstName, $lastName, $email, $password, $salt, $isAdmin, $dietarySpecifics);
	$statement->fetch();
?> 
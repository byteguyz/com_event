<?php 
	//select and bind all data from the events table to local variables
	$statement = $db->prepare("SELECT * FROM Events WHERE eventID = ?");
	$statement->bind_param('d', $eventID);	
	$statement->execute();
	$statement->store_result();
	$statement->bind_result($eventID, $eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $eventHidden, $imageThumbnail, $imageHeader);
	$statement->fetch();
?> 
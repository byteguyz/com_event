<?php
	//register the event into the database
	function registerEvent($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr) {
		$db = new mysqli('localhost', 'byteguyz_admin', 'Redfred123$', 'byteguyz_community');

		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		$stmt = $db->prepare("INSERT INTO Events (eventName, eventDate, eventLocation, eventCapacity, eventMemberCost, eventInfo, eventSummary, eventHidden, imageHeader) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('sssddssds', $eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr);
		$stmt->execute();
	}	
	//register the members event into the database
	function registerEventMembers($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr) {
		$db = new mysqli('localhost', 'byteguyz_admin', 'Redfred123$', 'byteguyz_community');

		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		$stmt = $db->prepare("INSERT INTO MemberEvents (eventName, eventDate, eventLocation, eventCapacity, eventMemberCost, eventInfo, eventSummary, eventHidden, imageHeader) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('sssddssds', $eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr);
		$stmt->execute();
	}
	//edit the event in the database
	function updateEvents($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $eventID, $privateEvent, $target_file_addr) {
		$db = new mysqli('localhost', 'byteguyz_admin', 'Redfred123$', 'byteguyz_community');

		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		$stmt = $db->prepare("UPDATE Events SET eventName = ?, eventDate = ?, eventLocation = ?, eventCapacity = ?, eventMemberCost = ?, eventInfo = ?, eventSummary = ?, eventHidden = ? , imageHeader = ? WHERE eventID = ? " );
		$stmt->bind_param('sssddssdsd', $eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr, $eventID);
		$stmt->execute();
	}
?>
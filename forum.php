<?php
	session_start();
	require 'includes/connect.inc';
    if (!isset($_SESSION['isAdmin'])) {
		header('Location: http://byteguyz.org');
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
        <span style="display:none;">***</span>
        <script type="text/javascript" language="javascript" id="pnyxeForumItJs" src="https://www.pnyxe.shadow.com/PnyxeForumItJs.jsp"></script>
        <script type="text/javascript" language="javascript" id="pnyxeForumItInitJs530823">try { var zpbw_webWidgetClientKey = "F0rIgakYh_hQnCtCk7YXMA"; var pnyxeForumIt = new PnyxeForumIt(); pnyxeForumIt.init("530823"); } catch (e) {}</script>
        <noscript><a href="https://www.pnyxe.shadow.com/DiscussIt-comment-system?utm_source=wwcCodeSpanPromotion3" rel="nofollow">Free Comment Box</a></noscript>
        
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>

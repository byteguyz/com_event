<?php
	session_start();
	require 'includes/connect.inc';
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
        <section id="text_columns">
			<!-- website blog -->
            <article class="column1">
				<div class="blogPicture"  style="height:200px;">
						<img src="images/banner.png" />
				</div>
				<?php
				$sql = "SELECT blogsID, blogName, blogSummary FROM Blogs ORDER BY blogDate ASC LIMIT 2";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='blogText'>";
								echo "<h3>" . $row['blogName'] . "</h3>";
								echo "<p>" . $row['blogSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/blogInfo.php?blogsID=" . $row['blogsID'] . "'>More info</a></p>";
							echo "</div>";
						}
					}
				?>
				<div id="viewAll">
					<a href="blog.php">View All</a>
				</div>
			</article>
			<!-- list of upcoming events -->
            <article class="column2">  
				<div id="upcomingEvents">
					<h2>Upcoming Events</h2>
				</div>				
				<?php 
					//select first four events that are closest to the current date and display the data
					$sql = "SELECT eventID, eventName, eventSummary, imageHeader FROM Events WHERE eventDate > CURDATE() AND (eventHidden IS NULL OR eventHidden = 0) ORDER BY eventDate ASC LIMIT 4";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='event'>";
							echo "<div class='eventText'>";
								echo "<h3>" . $row['eventName'] . "</h3>";
								echo "<p>" . $row['eventSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/eventInfo.php?eventID=" . $row['eventID'] . "'>More info</a></p>";
							echo "</div>";
							echo "<div class='eventPicture' style='position:relative; overflow: hidden;'>";
								//echo "<img src='http://dummyimage.com/254x170/000000/fff.png' />";
								if ($row['imageHeader'] == ''){
								echo"<img src='/images/499055836.jpg' style='height:100%;' />";
								}
								else{
									echo"<img src='". $row['imageHeader'] . "' style='height:100%; position: absolute; top:-9999px; bottom:-9999px; left:-9999px; right:-9999px; margin: auto;' />";
								}
								
								echo "</div>";
							echo "</div>";
						}
					}
				?>
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>

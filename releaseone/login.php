<?php
	session_start();
	require 'includes/validation.inc';
	require 'includes/userFunctions.inc';	
	$errMsg = '';
	
	//if user submits the form to login, validate submission and compare with database
	if(isset($_POST['login'])) {
		$username = validate($_POST['username']);
		$password = validate($_POST['password']);
		if($username == '') {
			$errMsg .= 'You must enter your Username<br>';
		}
		elseif (!preg_match("/^[a-zA-Z_-]{3,15}$/", $username)) {
			$errMsg .= 'Username can contain only letters with a length between 3-15 characters<br>';
		}			
		if($password == '') {
			$errMsg .= 'You must enter your Password<br>';
		}
		
		//if user input passes validation, check for a match in database
		if (!$errMsg) {
			require 'includes/connect.inc';
			$statement = $db->prepare("SELECT usersID, username, password, isAdmin FROM Users WHERE username = ? and password = SHA2(CONCAT(?, salt), 0)");
			$statement->bind_param('ss', $username, $password);	
			$statement->execute();
			$statement->store_result();
			$statement->bind_result($usersID, $username, $password, $isAdmin);
			$statement->fetch();
			if ($statement->num_rows > 0) {
				//if there is a match with the user and they are an admin, assign associated session
				if ($isAdmin == 0) {
					session_start();
					$_SESSION['isUser'] = true;
					$_SESSION['usersID'] = $usersID;
					$errMsg .= 'Logged in as user';
				}
				//if there is a match with the user and they aren't an admin, assign associated session
				if ($isAdmin == 1) {
					session_start();
					$_SESSION['isAdmin'] = true;
					$_SESSION['usersID'] = $usersID;
					$errMsg .= 'Logged in as admin';
				}
			}
			else {
				$errMsg .= 'Incorrect username or password';
			}
		}
	}
	//if user registers an account, validate submission and insert into database
	if(isset($_POST['register'])) {
		$errMsgRegister = '';
		$username = validate($_POST['username']);
		$firstName = validate($_POST['firstName']);
		$lastName =  validate($_POST['lastName']);
		$email =  validate($_POST['email']);
		$password =  validate($_POST['password']);
		
		//validate the user input utilising regular expressions
		if($username == '') {
			$errMsgRegister .= 'You must enter your username<br>';
		}
		elseif (!preg_match("/^[a-zA-Z_-]{3,15}$/", $username)) {
			$errMsgRegister .= 'Username can contain only letters with a length between 3-15 characters<br>';
		}		
		if($firstName == '') {
			$errMsgRegister .= 'You must enter your first name<br>';
		}
		elseif (!preg_match("/^[a-zA-Z]*$/", $firstName)) {
            $errMsgRegister = 'Only letters are allowed for your first name<br>'; 
        }
		if($lastName == '') {
			$errMsgRegister .= 'You must enter your last name<br>';
		}
		elseif (!preg_match("/^[a-zA-Z ]*$/", $lastName)) {
            $errMsgRegister = 'Only letters are allowed for your last name<br>'; 
        }
		if($email == '') {
			$errMsgRegister .= 'You must enter your email<br>';
		}
		elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errMsgRegister .= 'Insert an email with the format, example@host.com<br>'; 
        }
		if($password == '') {
			$errMsgRegister .= 'You must enter your password<br>';
		}
		if (strlen($password) <= '5') {
			$errMsgRegister .= 'Your password must contain at least five characters<br>';
		}
		elseif(!preg_match("#[0-9]+#",$password)) {
			$errMsgRegister .= 'Your password must contain at least one number<br>';
		}
		elseif(!preg_match("#[A-Z]+#",$password)) {
			$errMsgRegister .= 'Your password must contain at least one capital letter<br>';
		}
		//if validation is passed, register user account to database
		if (!$errMsgRegister) {
			signUp($username, $firstName, $lastName, $email, $password);	
			$errMsgRegister .= 'Registered account, please login';
		}
	}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
			<!-- user login form -->
            <article class="column1">
				<div class="formStyle">
					<h2>Login to your account</h2>
					<form action="login.php" method="POST">
						<div class="requiredField">
							<input name="username" id="username" type="text" placeholder="Username" value="<?php if(isset($_POST['login'])){ echo $_POST['username'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="password" id="password" type="password" placeholder="Password" required/>
						</div>
						<input type="submit" name="login" value="Login"/>
						<span id="errorField" class="errorField">
							<?php 
								echo $errMsg; 
							?>
						</span>
					</form>
				</div>
			</article>
			<!-- user registration form -->
            <article class="column2">  
				<div class="formStyle">
					<h2>Register your account</h2>
					<form action="login.php" method="POST">
						<div class="requiredField">
							<input name="username" id="username" type="text" placeholder="Username" value="<?php if(isset($_POST['register'])){ echo $_POST['username'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="firstName" id="firstName" type="text" placeholder="First Name" value="<?php if(isset($_POST['register'])){ echo $_POST['firstName'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="lastName" id="lastName" type="text" placeholder="Last Name" value="<?php if(isset($_POST['register'])){ echo $_POST['lastName'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="email" id="email" type="email" placeholder="Email Adress" value="<?php if(isset($_POST['register'])){ echo $_POST['email'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="password" id="password" type="password" placeholder="Password" required/>
						</div>
						<input type="submit" name="register" value="Register"/>
						<span id="errorField" class="errorField">
							<?php 
								echo $errMsgRegister; 
							?>
						</span>
					</form>
				</div>
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
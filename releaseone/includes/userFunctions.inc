<?php
	//check that the username and password input is the same as the one in the database
	function checkPassword(&$results, $username, $password) {	
		require 'includes/connect.inc';

		$statement = $db->prepare("SELECT username, password, isAdmin FROM Users WHERE username = ? and password = SHA2(CONCAT(?, salt), 0)");
		$statement->bind_param('ss', $username, $password);	
		$statement->execute();
		$statement->store_result();
		$statement->bind_result($username, $password, $isAdmin);
		if ($statement->num_rows > 0) {
			return true;
		}
		else {
		
		}
	}	
	
	//insert the users details into the database to register their account
	function signUp($username, $firstName, $lastName, $email, $password) {
		require 'includes/connect.inc';
		$statement = $db->prepare("INSERT INTO Users (username, firstName, lastName, email, password, salt, isAdmin) VALUES (?, ?, ?, ?, SHA2(CONCAT(?, '4b3403665fea6'), 0), '4b3403665fea6', 0)");
		$statement->bind_param('sssss', $username, $firstName, $lastName, $email, $password);
		$statement->execute();
	}	

?>


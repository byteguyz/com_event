<header>
	<div class="toggleMobile">
		<span class="menu1"></span>
		<span class="menu2"></span>
		<span class="menu3"></span>
	</div>
	<div id="mobileMenu">
		<ul>
			<li class="events"><a href="events.php">Events</a></li>
			<li class="about"><a href="about.php">About</a></li>
			<li class="blog"><a href="blog.php">Blog</a></li>
			<?php
				if (!isset($_SESSION['isAdmin']) && !isset($_SESSION['isUser'])) {
					echo '<li class="login"><a href="login.php">Login</a></li>';
				}
				else {
					echo '<li class="logout"><a href="logout.php">Logout</a></li>';
				}
			?>
		</ul>
	</div>
	
	<div id="heading">
		<h1><a href="index.php">Community Events</a></h1>
	</div>
	<nav>
		<ul>
			<li class="events"><a href="events.php">Events</a></li>
			<li class="about"><a href="about.php">About</a></li>
			<li class="blog"><a href="blog.php">Blog</a></li>
			<?php
				if (!isset($_SESSION['isAdmin']) && !isset($_SESSION['isUser'])) {
					echo '<li class="login"><a href="login.php">Login</a></li>';
				}
				else {
					echo '<li class="logout"><a href="logout.php">Logout</a></li>';
				}
			?>
		</ul>
	</nav>
</header>
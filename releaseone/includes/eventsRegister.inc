<?php
	//register the event into the database
	function registerEvent($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary) {
		$db = new mysqli('localhost', 'byteguyz_admin', 'Redfred123$', 'byteguyz_community');

		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		$stmt = $db->prepare("INSERT INTO Events (eventName, eventDate, eventLocation, eventCapacity, eventMemberCost, eventInfo, eventSummary) VALUES (?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('sssddss', $eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary);
		$stmt->execute();
	}	
	//register the members event into the database
	function registerEventMembers($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary) {
		$db = new mysqli('localhost', 'byteguyz_admin', 'Redfred123$', 'byteguyz_community');

		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		$stmt = $db->prepare("INSERT INTO MemberEvents (eventName, eventDate, eventLocation, eventCapacity, eventMemberCost, eventInfo, eventSummary) VALUES (?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param('sssddss', $eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary);
		$stmt->execute();
	}
?>
<?php
	session_start();
	require 'includes/connect.inc';
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
        <section id="text_columns">
			<!-- website blog -->
            <article class="column1">
				<div class="blogPicture">
						<img src="images/banner.png" />
				</div>
				<div class="blogText">
					<h3>Blog Post</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor rutrum lacus suscipit fringilla. Maecenas in dolor id arcu commodo bibendum eu sit amet ligula. Vivamus dapibus id massa quis imperdiet. Donec vitae nisl purus.</p>
					<p>Maecenas venenatis condimentum lacus, non tincidunt enim lobortis quis. Donec consequat arcu sit amet dignissim semper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas tempor vestibulum dui, vitae finibus elit pellentesque ut.</p>
					<p><a href="">Read more</a></p>
				</div>
				<div class="blogText">
					<h3>Blog Post</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer porttitor rutrum lacus suscipit fringilla. Maecenas in dolor id arcu commodo bibendum eu sit amet ligula. Vivamus dapibus id massa quis imperdiet. Donec vitae nisl purus.</p>
					<p>Maecenas venenatis condimentum lacus, non tincidunt enim lobortis quis. Donec consequat arcu sit amet dignissim semper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas tempor vestibulum dui, vitae finibus elit pellentesque ut.</p>
					<p><a href="">Read more</a></p>
				</div>
				<div id="viewAll">
					<a href="">View All</a>
				</div>
			</article>
			<!-- list of upcoming events -->
            <article class="column2">  
				<div id="upcomingEvents">
					<h2>Upcoming Events</h2>
				</div>				
				<?php 
					//select first four events that are closest to the current date and display the data
					$sql = "SELECT eventID, eventName, eventSummary FROM Events WHERE eventDate > CURDATE() ORDER BY eventDate ASC LIMIT 4";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='event'>";
							echo "<div class='eventText'>";
								echo "<h3>" . $row['eventName'] . "</h3>";
								echo "<p>" . $row['eventSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/releaseone/eventInfo.php?eventID=" . $row['eventID'] . "'>More info</a></p>";
							echo "</div>";
							echo "<div class='eventPicture'>";
								echo "<img src='http://dummyimage.com/254x170/000000/fff.png' />";
								echo "</div>";
							echo "</div>";
						}
					}
				?>
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>

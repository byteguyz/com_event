<?php
	session_start();
	if (!isset($_SESSION['isAdmin']) || !isset($_SESSION['isUser'])) {
		header('Location: http://byteguyz.org/releaseone/index.php');
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
		<?php require 'includes/connect.inc'; ?>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				
			</article>
			
            <article class="column2">
				
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
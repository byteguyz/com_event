<?php
	session_start();
	require 'includes/connect.inc';
	require 'includes/validation.inc';
	$eventID = $_GET['eventID'];
	$usersID = $_SESSION['usersID'];
	require 'includes/eventInformation.inc';
	
	//if user makes a comment, insert into database
	if(isset($_POST['login'])) {
		$errMsg = '';
		$userComment = validate($_POST['userComment']);
	
		if($userComment == '') {
			$errMsg .= 'You must enter your comment<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{20,150}/", $userComment)) {
			$errMsg .= 'Your comment must be between 20 to 150 characters, and only include common punctuation<br>';
		}
		if (!$errMsg) {
			require 'includes/userComments.inc';
		}
	}
	//if user purchases ticket, assign user to event, email notification
	if(isset($_POST['purchase'])) {
		//insert user into JoinedEvents
		$statement = $db->prepare("INSERT INTO JoinedEvents (eventID, usersID) VALUES (?, ?)");
		$statement->bind_param('dd', $eventID, $usersID);
		$statement->execute();
		
		//select users name and email in preperation for the email
		$statement = $db->prepare("SELECT username, email FROM Users WHERE usersID = ?");
		$statement->bind_param('d', $usersID);	
		$statement->execute();
		$statement->store_result();
		$statement->bind_result($username, $email);
		$statement->fetch();
		
		//email the user a confirmation of joining the event
		$emailto = $email;
		$toname = $username;
		$emailfrom = 'mail.byteguyz.org';
		$fromname = 'Admin';
		$subject = 'Invite Confirmation';
		$messagebody = "Greetings $username,\n\nYou have joined the event, $eventName, which takes placed on $eventDate. We look forward to seeing you!";
		$headers = 
			'Return-Path: ' . $emailfrom . "\r\n" . 
			'From: ' . $fromname . ' <' . $emailfrom . '>' . "\r\n" . 
			'X-Priority: 3' . "\r\n" . 
			'X-Mailer: PHP ' . phpversion() .  "\r\n" . 
			'Reply-To: ' . $fromname . ' <' . $emailfrom . '>' . "\r\n" .
			'MIME-Version: 1.0' . "\r\n" . 
			'Content-Transfer-Encoding: 8bit' . "\r\n" . 
			'Content-Type: text/plain; charset=UTF-8' . "\r\n";
		$params = '-f ' . $emailfrom;
		$test = mail($emailto, $subject, $messagebody, $headers, $params);
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta property="og:url" content="<?php echo "http://byteguyz.org/releaseone/eventInfo.php?eventID=$eventID"; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="<?php echo $eventName; ?>" />
		<meta property="og:description" content="<?php echo $eventSummary; ?>" />
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>	
	</head>

	<body>
		<!-- facebook api for share button -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.7";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	
        <?php require 'includes/header.inc'; ?>
        <section id="text_columns">
            <article class="column1">
				<div class="blogPicture">
					<img src="http://dummyimage.com/685x200/000000/fff.png" />
				</div>
				<?php 
					echo "<div class='blogText'>";
						echo "<h3>" . $eventName . "</h3>";
						echo "<p>" . $eventInfo . "</p>";
						echo "<p><b>Date: </b>" . date("M jS, Y", strtotime("$eventDate")) . "</p>";
						echo "<p><b>Location: </b>" . $eventLocation . "</p>";
						
						//get the count of users attending the event and the total capacity for the event
						$statement = $db->prepare("Select count(j.usersID) AS attending, e.eventCapacity FROM JoinedEvents AS j, Events AS e WHERE j.eventID = ? AND e.eventID = ?");
						$statement->bind_param('dd', $eventID, $eventID);	
						$statement->execute();
						$statement->store_result();
						$statement->bind_result($attending, $capacity);
						$statement->fetch();
						$reduced = $capacity - $attending;
						$totalCost = $eventMemberCost * $capacity;
						echo "<p><b>Tickets Remaining: </b>" . $reduced . "/$eventCapacity</p>";
						echo "<p><b>Ticket Cost:</b> $" . number_format((float)$eventMemberCost, 2, '.', '') . "</p>";
						//if the user is an admin, display total cost of the event
						if (isset($_SESSION['isAdmin'])) {
							echo "<p><b>Total Cost:</b> $" . number_format((float)$totalCost, 2, '.', '') . "</p>";
						}
						
						//if the total attending is less then the capacity, allow members to join the event
						if ($attending < $capacity) {
							if (isset($_SESSION['isAdmin']) || isset($_SESSION['isUser'])) {
								$statement = $db->prepare("SELECT * FROM JoinedEvents WHERE eventID = ? AND usersID = ?");
								$statement->bind_param('dd', $eventID, $usersID);	
								$statement->execute();
								$statement->store_result();
								$statement->bind_result($eventID, $usersID);
								$statement->fetch();
								
								//if users have joined the event already, do not display the button to purchase a ticket
								if ($statement->num_rows == 0) {
									echo "<form action='http://byteguyz.org/releaseone/eventInfo.php?eventID=$eventID' method='POST' id='eventForm'>";
										echo "<input type='submit' name='purchase' value='Purchase Ticket'/>";
									echo "</form>";
								}
							}
						}
						else {
							echo "<p>Event is full</p>";
						}
						echo "<div class='fb-share-button' 
							data-href='http://byteguyz.org/releaseone/eventInfo.php?eventID=" . $eventID . "' 
							data-layout='button_count'>
						</div>";
					echo "</div>";
				?>
				<div class="blogText">
					<div id="memberList">
						<h3>Members Attending:</h3>
						<ul>
							<?php
								//display a list of all users who have joined the event
								$statement = $db->prepare("SELECT u.firstName, u.lastName FROM JoinedEvents AS j, Users AS u WHERE j.eventID = ? AND u.usersID = j.usersID");
								$statement->bind_param('d', $eventID);
								$statement->execute();
								$statement->store_result();
								$statement->bind_result($firstName, $lastName);
								$i = 1;
								while ($statement->fetch()) {
									echo "<li>$i. $firstName $lastName</li>";
									$i = $i + 1;
								}
							?>
						</ul>
					</div>
				</div>
			</article>
			
            <article class="column2">
				<?php
					//display a list of all comments that users have made on the event
					$statement = $db->prepare("SELECT c.comment, u.username FROM Comments AS c, Users AS u WHERE c.eventId = ? AND c.usersID = u.usersID");
					$statement->bind_param('d', $eventID);	
					$statement->execute();
					$statement->store_result();
					$statement->bind_result($userComment, $username);
					$statement->fetch();
					
					//if there are no comments, don't display comment's div
					if ($statement->num_rows > 0) {
						$statement = $db->prepare("SELECT c.comment, u.username FROM Comments AS c, Users AS u WHERE c.eventId = ? AND c.usersID = u.usersID");
						$statement->bind_param('d', $eventID);	
						$statement->execute();
						$statement->store_result();
						$statement->bind_result($userComment, $username);
						echo "<div id='upcomingEvents'>";
							echo "<h2 class='comments'>Comments</h2>";
						echo "</div>";
						echo "<div class='blogText'>";
						while ($statement->fetch()) {
							echo "<h4>" . $username . "</h4>";
							echo "<p>" . $userComment . "</p>";
							
						}
						echo "</div>";
					}
					//if the user is logged in, give the ability to make a comment
					if (isset($_SESSION['isAdmin']) || isset($_SESSION['isUser'])) {
						echo "<div class='formStyle'>";
							echo "<h2 class='comments'>Write a comment</h2>";
							echo "<form action='http://byteguyz.org/releaseone/eventInfo.php?eventID=" . $eventID . "' method='POST' id='eventInfo'>";
								echo "<div class='requiredField'>";
									echo "<textarea rows='4' cols='50' name='userComment' id='userComment' form='eventInfo' placeholder='Write a comment...' required>";
										if(isset($_POST['login'])){
											echo $_POST['userComment'];
										}
									echo "</textarea>";
								echo "</div>";
								echo "<input type='submit' name='login' value='Comment'/>";
								echo "<span id='errorField' class='errorField'>";
									echo $errMsg; 
								echo "</span>";
							echo "</form>";
						echo "</div>";
					}
				?>
			</article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>

<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
		<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.css' />
		<script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js'></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.js'></script>
	</head>

	<body>
		<?php require 'includes/connect.inc'; ?>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
			<br>
			<br>
			<?php
				require 'includes/connect.inc';
				$sql = "SELECT eventID, eventName, eventDate FROM Events WHERE eventHidden IS NULL OR eventHidden = 0";
				$data = $db->query($sql);
				if ($data->num_rows > 0) {
					echo "<script>";
						echo "$(document).ready(function() {";
							echo "$('#calendar').fullCalendar({";
								// emphasizes business hours
								echo "businessHours: true,";
								echo "events: [";
								while($row = $data->fetch_assoc()) {	
									echo "{";
										echo "title: '" . $row['eventName'] . "',";
										echo "start: '" . $row['eventDate'] . "',";
										echo "url: 'http://byteguyz.org/eventInfo.php?eventID=" . $row['eventID'] . "'"; 
									echo "},";
								}
								echo "]";
							echo "})";
						echo "});";
					echo "</script>";
				}
			?>				
			<div id='calendar'></div>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
<?php
	session_start();
	require 'includes/eventsRegister.inc';
	require 'includes/validation.inc';
	require 'includes/connect.inc';
	
	if (!isset($_SESSION['isAdmin']) && !isset($_SESSION['isUser'])) {
		header('Location: http://byteguyz.org');
	}
	
	//if the admin submits an event, run through validation and submit to database
	if(isset($_POST['login'])) {
		$errMsg = '';
		$target_dir = "images/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$target_dir_addr = "/images/";
		$target_file_addr = $target_dir_addr . basename($_FILES["fileToUpload"]["name"]);
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		$eventName = validate($_POST['eventName']);
		$eventDate = validate($_POST['eventDate']);
		$eventLocation = validate($_POST['eventLocation']);
		$eventCapacity = validate($_POST['eventCapacity']);
		$eventMemberCost = validate($_POST['eventMemberCost']);
		$eventInfo = validate($_POST['eventInfo']);
		$eventInfo = nl2br($eventInfo);
		$eventSummary = validate($_POST['eventSummary']);
		$eventSummary = nl2br($eventSummary);
		$privateEvent = $_POST['privateEvent'];
		if ($privateEvent) {
			$privateEvent = 1;
		} else {
			$privateEvent = 0;
		}
		
		if($eventName == '') {
			$errMsg .= 'You must enter your event name<br>';
		}
		elseif (!preg_match("/^[a-zA-Z ]{5,40}$/", $eventName)) {
			$errMsg .= 'Your event name must contain only letters and be between 5 to 40 characters in length<br>';
		}		
		if($eventDate == '') {
			$errMsg .= 'You must enter your date<br>';
		}
		if($eventLocation == '') {
			$errMsg .= 'You must enter your location<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9, ]{5,50}/", $eventLocation)) {
			$errMsg .= 'Your location must contain only alphanumeric characters and be between 5 to 50 characters in length<br>';
		}
		if($eventCapacity == '') {
			$errMsg .= 'You must enter your capacity<br>';
		}
		elseif (!preg_match("/^[0-9]{2,5}$/", $eventCapacity)) {
			$errMsg .= 'Your event capacity must contain only numbers and be between two to five characters in length<br>';
		}
		if($eventMemberCost == '') {
			$errMsg .= 'You must enter your cost<br>';
		}
		elseif (!preg_match("/^[0-9.]*$/", $eventMemberCost)) {
			$errMsg .= 'Your event member cost must contain only numbers and a decimal<br>';
		}
		if($eventInfo == '') {
			$errMsg .= 'You must enter your information on the event<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{100,}/", $eventInfo)) {
			$errMsg .= 'Your event information must be greater than 100 characters, and only include common punctuation<br>';
		}
		if($eventSummary == '') {
			$errMsg .= 'You must enter your summary on the event<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{100,150}/", $eventSummary)) {
			$errMsg .= 'Your summary must be between 100 to 150 characters, and only include common punctuation<br>';
		}
		

		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 3000000) {
			$errMsg .= 'Sorry, your file is too large. Your file should be under 60kb';

		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$errMsg .= 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';

		}

		
		//if the form input passes validation, submit to database
		if (!$errMsg) {
			//if the form was submitted by an admin, submit directly to the events table
			if (isset($_SESSION['isAdmin'])) {
				registerEvent($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr);	
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
			}
			//if the form was submitted by a user, make the event wait for admin approval
			if (isset($_SESSION['isUser'])) {
				registerEventMembers($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $privateEvent, $target_file_addr);	
			
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
			}
			header('Location: http://byteguyz.org/events.php');
		}
	}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>
	
	<body>
		<?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<div class="formStyle">
					<h2>Create an Event</h2>
					<form action="eventsForm.php" method="POST" id="eventForm" enctype="multipart/form-data">
						<div class="requiredField">
							<input name="eventName" id="eventName" type="text" placeholder="Name" value="<?php if(isset($_POST['login'])){ echo $_POST['eventName'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventDate" id="eventDate" type="date" placeholder="yyyy-mm-dd"  min="<?php echo date('Y-m-d'); ?>" value="<?php if(isset($_POST['login'])){ echo $_POST['eventDate'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventLocation" id="eventLocation" type="text" placeholder="Location" value="<?php if(isset($_POST['login'])){ echo $_POST['eventLocation'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventCapacity" id="eventCapacity" type="text" placeholder="Capacity" value="<?php if(isset($_POST['login'])){ echo $_POST['eventCapacity'];}?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventMemberCost" id="eventMemberCost" type="text" placeholder="Cost" value="<?php if(isset($_POST['login'])){ echo $_POST['eventMemberCost'];}?>" required/>
						</div>
						<div class="requiredField">
							<textarea rows="4" cols="50" name="eventInfo" id="eventInfo" form="eventForm" placeholder="Event Information" required><?php if(isset($_POST['login'])){ echo $_POST['eventInfo'];}?></textarea>			
						</div>
						<div class="requiredField">
							<textarea rows="4" cols="50" name="eventSummary" id="eventSummary" form="eventForm" placeholder="Event Summary - between 100 to 150 characters"  required><?php if(isset($_POST['login'])){ echo $_POST['eventSummary'];}?></textarea>			
						</div>
						<div class="requiredField">
							<label for="privateEvent">Keep Event Hidden:</label>
							<input id="privateEvent" type="checkbox" name="privateEvent" value="true">
							<br>
							<label for="fileToUpload">Upload image:</label>
							<input type="file" name="fileToUpload" id="fileToUpload">
						</div>
						<input type="submit" name="login" value="Create"/>
						<span id="errorField" class="errorField">
							<?php 
								echo $errMsg; 
							?>
						</span>
					</form>
				</div>
			</article>
		</section>
		<?php require 'includes/footer.inc'; ?>
	</body>
</html>
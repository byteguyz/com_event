<?php
	session_start();
	require 'includes/connect.inc';
	require 'includes/validation.inc';
	$usersID = $_SESSION['usersID'];
	require 'includes/userInformation.inc';
	
	//if user registers an account, validate submission and insert into database
	if(isset($_POST['register'])) {
		$errMsgRegister = '';
		$username = validate($_POST['username']);
		$firstName = validate($_POST['firstName']);
		$lastName =  validate($_POST['lastName']);
		$email =  validate($_POST['email']);
		$dietarySpecifics = validate($_POST['dietarySpecifics']);
		$dietaryNeeds = $_POST['dietaryNeeds'];
		
		//validate the user input utilising regular expressions
		if($username == '') {
			$errMsgRegister .= 'You must enter your username<br>';
		}
		elseif (!preg_match("/^[a-zA-Z_-]{3,15}$/", $username)) {
			$errMsgRegister .= 'Username can contain only letters with a length between 3-15 characters<br>';
		}		
		if($firstName == '') {
			$errMsgRegister .= 'You must enter your first name<br>';
		}
		elseif (!preg_match("/^[a-zA-Z]*$/", $firstName)) {
            $errMsgRegister = 'Only letters are allowed for your first name<br>'; 
        }
		if($lastName == '') {
			$errMsgRegister .= 'You must enter your last name<br>';
		}
		elseif (!preg_match("/^[a-zA-Z ]*$/", $lastName)) {
            $errMsgRegister = 'Only letters are allowed for your last name<br>'; 
        }
		if($email == '') {
			$errMsgRegister .= 'You must enter your email<br>';
		}
		elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errMsgRegister .= 'Insert an email with the format, example@host.com<br>'; 
        }
		if($dietarySpecifics == '' && $dietaryNeeds) {
			$errMsgRegister .= 'You must describe the details of your dietary needs<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{1,150}/", $dietarySpecifics) && $dietaryNeeds) {
			$errMsgRegister .= 'Your dietary issues must be between 1 to 150 characters, and only include common punctuation<br>';
		}
		//if validation is passed, register user account to database
		if (!$errMsgRegister) {
			require 'includes/connect.inc';
			$stmt = $db->prepare("UPDATE Users SET username = ?, firstName = ?, lastName = ?, email = ?, dietarySpecifics = ? WHERE usersID = ?");
			$stmt->bind_param('sssssd', $username, $firstName, $lastName, $email, $dietarySpecifics, $usersID);
			$stmt->execute();	
			$errMsgRegister .= 'Updated account with new information';
		}
	}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
		<script type="text/javascript" src="functions.js"></script>
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
			<!-- user edit form -->
            <article class="column1">
				<div class="formStyle">
					<h2>Edit your account</h2>
					<form action="accountEdit.php" method="POST" id="registrationForm">
						<div class="requiredField">
							<input name="username" id="username" type="text" placeholder="Username" value="<?php if(isset($_POST['register'])){ echo $_POST['username'];} else { echo $username;}?>" required/>
						</div>
						<div class="requiredField">
							<input name="firstName" id="firstName" type="text" placeholder="First Name" value="<?php if(isset($_POST['register'])){ echo $_POST['firstName'];} else { echo $firstName;}?>" required/>
						</div>
						<div class="requiredField">
							<input name="lastName" id="lastName" type="text" placeholder="Last Name" value="<?php if(isset($_POST['register'])){ echo $_POST['lastName'];} else { echo $lastName;}?>" required/>
						</div>
						<div class="requiredField">
							<label for="dietaryNeeds">Dietary Needs:</label>
							<input id="dietaryNeeds" type="checkbox" onchange="showDetailInput()" name="dietaryNeeds">
							<div id="dietaryInputDetailDiv" class="dietaryInputDetailDiv" style="visibility: hidden;">
								<textarea rows="2" cols="50" name="dietarySpecifics" id="dietarySpecifics" form="registrationForm" placeholder="Specify your dietary requirements (Peanut allergy, lactose intolerant, Halal meats etc.)"><?php if(isset($_POST['register'])){ echo $_POST['dietarySpecifics'];} else { echo $dietarySpecifics;}?></textarea>
							</div>
						</div>
						<div class="requiredField">
							<input name="email" id="email" type="email" placeholder="Email Adress" value="<?php if(isset($_POST['register'])){ echo $_POST['email'];} else { echo $email;}?>" required/>
						</div>
						<input type="submit" name="register" value="Update Account"/>
						<span id="errorField" class="errorField">
							<?php 
								echo $errMsgRegister; 
							?>
						</span>
					</form>
				</div>
			</article>
            <article class="column2">  
				
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
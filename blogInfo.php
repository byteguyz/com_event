<?php
	session_start();
	require 'includes/connect.inc';
	require 'includes/validation.inc';
	$blogsID = $_GET['blogsID'];
	$usersID = $_SESSION['usersID'];
	require 'includes/blogInformation.inc';
	
	//if user makes a comment, insert into database
	if(isset($_POST['login'])) {
		$errMsg = '';
		$userComment = validate($_POST['userComment']);
	
		if($userComment == '') {
			$errMsg .= 'You must enter your comment<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{20,150}/", $userComment)) {
			$errMsg .= 'Your comment must be between 20 to 150 characters, and only include common punctuation<br>';
		}
		if (!$errMsg) {
			//insert users comment into the database
			$stmt = $db->prepare("INSERT INTO BlogsComments (usersID, blogsID, comments) VALUES (?, ?, ?)");
			$stmt->bind_param('dds', $usersID, $blogsID, $userComment);
			$stmt->execute();
		}
	}
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta property="og:url" content="<?php echo "http://byteguyz.org/eventInfo.php?eventID=$eventID"; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="<?php echo $eventName; ?>" />
		<meta property="og:description" content="<?php echo $eventSummary; ?>" />
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>	
	</head>

	<body>
        <?php require 'includes/header.inc'; ?>
        <section id="text_columns">
            <article class="column1">
				<?php
				//<div class="blogPicture">
				echo "<div class='blogPicture' style='position:relative; overflow: hidden;'>";
						
								if ($imageHeader == ''){
					echo"<img src='/images/499055836.jpg' style='width:100%;' />";
					}
					else{
						echo"<img src='". $imageHeader . "' style='width:100%; position: absolute; top:-9999px; bottom:-9999px; left:-9999px; right:-9999px; margin: auto;' />";
					}
				echo"</div>";
				?>
				<?php 
					echo "<div class='blogText'>";
						echo "<h3>" . $blogName . "</h3>";
						echo "<p><b>Date: </b>" . date("M jS, Y", strtotime("$blogDate")) . "</p>";
						echo "<p>" . $blogInfo . "</p>";
					echo "</div>";
				?>
			</article>
			
            <article class="column2">
				<?php
					//display a list of all comments that users have made on the event
					$statement = $db->prepare("SELECT b.comments, u.username FROM BlogsComments AS b, Users AS u WHERE b.blogsID = ? AND b.usersID = u.usersID");
					$statement->bind_param('d', $blogsID);	
					$statement->execute();
					$statement->store_result();
					$statement->bind_result($userComment, $username);
					$statement->fetch();
					
					//if there are no comments, don't display comment's div
					if ($statement->num_rows > 0) {
						$statement = $db->prepare("SELECT b.comments, u.username FROM BlogsComments AS b, Users AS u WHERE b.blogsID = ? AND b.usersID = u.usersID");
						$statement->bind_param('d', $blogsID);	
						$statement->execute();
						$statement->store_result();
						$statement->bind_result($userComment, $username);
						echo "<div id='upcomingEvents'>";
							echo "<h2 class='comments'>Comments</h2>";
						echo "</div>";
						echo "<div class='blogText'>";
						while ($statement->fetch()) {
							echo "<h4>" . $username . "</h4>";
							echo "<p>" . $userComment . "</p>";
						}
						echo "</div>";
					}
					//if the user is logged in, give the ability to make a comment
					if (isset($_SESSION['isAdmin']) || isset($_SESSION['isUser'])) {
						echo "<div class='formStyle'>";
							echo "<h2 class='comments'>Write a comment</h2>";
							echo "<form action='http://byteguyz.org/blogInfo.php?blogsID=" . $blogsID . "' method='POST' id='eventInfo'>";
								echo "<div class='requiredField'>";
									echo "<textarea rows='4' cols='50' name='userComment' id='userComment' form='eventInfo' placeholder='Write a comment...' required>";
										if(isset($_POST['login'])){
											echo $_POST['userComment'];
										}
									echo "</textarea>";
								echo "</div>";
								echo "<input type='submit' name='login' value='Comment'/>";
								echo "<span id='errorField' class='errorField'>";
									echo $errMsg; 
								echo "</span>";
							echo "</form>";
						echo "</div>";
					}
				?>
			</article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>

<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
		<?php require 'includes/connect.inc'; ?>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<div class='blogText'>
					<h3>About</h3>			
					<p>This website aims to bring together the local community with information on events that highlight the best parts of the area. Join online events for updates on events with cuisine, music, games and other activities meant to bring everyone together for a great time out. Become a member to join and receive notifications so you can stay up to date!</p>
				</div>
				<div class='blogText'>
					<h3>FAQ:</h3>	
					<h4>How do I join events?</h4>
					<p>Click ‘more info’ on the event page under the event you would like to attend, and click ‘purchase ticket’ to join. Make sure you’re a member first!</p>

					<h4>How do I become a member?</h4>
					<p>Just click ‘login’ at the top right corner!</p>

					<h4>Can I change my account details?</h4>
					<p>Yes! Go to ‘profile’, then ‘edit account’.</p>

					<h4>How can I let the event know I have dietary requirements?</h4>
					<p>Make sure you’ve ticked that you have dietary requirements in your profile and stated what you need. These details are automatically added to the event you’ve joined.</p>

					<h4>I recently purchased a ticket but now the price is lower; why?</h4>
					<p>When other members donate towards the event it can lower the price of the ticket for future members.</p>

					<h4>Can I share this to social media?</h4>
					<p>You can click the Facebook share button within every event to share with friends what you’re up to.</p>
					
				</div>
			</article>
			
            <article class="column2">
				<div class='blogText'>
					<h3>Event Creation FAQ:</h3>	
					<h4>Can I add events?</h4>
					<p>You sure can and you can even make them private. After you go to the events tab, click ‘add event for approval’ in the green box and you can submit a request to make an event.</p>
					
					<h4>I’ve made an event but it doesn’t show up?</h4>
					<p>That’s most likely because it’s awaiting admin approval. This is so that we don’t have people hijacking the website and overloading it with spam.</p>
					
					<h4>What should I aim to do when creating an event?</h4>
					<p>Make sure you include all the required information possible, from location, date, to detailed information about the event. The more you provide, the higher chance of success of approval.</p>

					<h4>What sort of images are allowed?</h4>
					<p>All common file formats for images are allowed, it is recommended you use a nice large image, as our website will scale it down appropriately for both the header and thumbnail.</p>
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
<?php
	session_start();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
		<?php require 'includes/connect.inc'; ?>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<?php 
					//if the user is an admin, display ability to add and approve events
					if (isset($_SESSION['isAdmin'])) {
						echo "<div id='addEvents'>";
							echo "<a href='eventsForm.php'>Admin: Add Events</a>";
						echo "</div>";	
						echo "<div id='addEvents'>";
							echo "<a href='eventsApproval.php'>Admin: Approve Events</a>";
						echo "</div>";
						echo "<div id='addEvents'>";
							echo "<a href='eventsHidden.php'>Admin: Hidden Events</a>";
						echo "</div>";
					}
					//if the user is not an admin, display ability to add events for approval
					if (isset($_SESSION['isUser'])) {
						echo "<div id='addEvents'>";
							echo "<a href='eventsForm.php'>Add Events for Approval</a>";
						echo "</div>";	
					}
					//display the first four upcoming events based on the current date
					$sql = "SELECT eventID, eventName, eventSummary, imageHeader FROM Events WHERE eventDate > CURDATE() AND (eventHidden IS NULL OR eventHidden = 0) ORDER BY eventDate ASC LIMIT 4";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='event'>";
							echo "<div class='eventText'>";
								echo "<h3>" . $row['eventName'] . "</h3>";
								echo "<p>" . $row['eventSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/eventInfo.php?eventID=" . $row['eventID'] . "'>More info</a></p>";
							echo "</div>";
							echo "<div class='eventPicture' style='position:relative; overflow: hidden;'>";
								//echo "<img src='http://dummyimage.com/254x170/000000/fff.png' />";
								if ($row['imageHeader'] == ''){
								echo"<img src='/images/499055836.jpg' style='height:100%;' />";
								}
								else{
									echo"<img src='". $row['imageHeader'] . "' style='height:100%; position: absolute; top:-9999px; bottom:-9999px; left:-9999px; right:-9999px; margin: auto;' />";
								}
								
								echo "</div>";
							echo "</div>";
						}
					}	
				?>
			</article>
			
            <article class="column2">
				<?php
					//display the next four upcoming events based on the current date
					$sql = "SELECT eventID, eventName, eventSummary, imageHeader FROM Events WHERE eventDate > CURDATE() AND (eventHidden IS NULL OR eventHidden = 0) ORDER BY eventDate ASC LIMIT 4, 4";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='event'>";
							echo "<div class='eventText'>";
								echo "<h3>" . $row['eventName'] . "</h3>";
								echo "<p>" . $row['eventSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/eventInfo.php?eventID=" . $row['eventID'] . "'>More info</a></p>";
							echo "</div>";
							echo "<div class='eventPicture' style='position:relative; overflow: hidden;'>";
								//echo "<img src='http://dummyimage.com/254x170/000000/fff.png' />";
								if ($row['imageHeader'] == ''){
								echo"<img src='/images/499055836.jpg' style='height:100%;' />";
								}
								else{
									echo"<img src='". $row['imageHeader'] . "' style='height:100%; position: absolute; top:-9999px; bottom:-9999px; left:-9999px; right:-9999px; margin: auto;' />";
								}
								
								echo "</div>";
							echo "</div>";
						}
					}	
				?>
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
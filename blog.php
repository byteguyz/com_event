<?php
	session_start();
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>

	<body>
		<?php require 'includes/connect.inc'; ?>
        <?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<?php
					//if the user is an admin, display ability to add and approve events
					if (isset($_SESSION['isAdmin'])) {
						echo "<div id='addEvents'>";
							echo "<a href='blogForm.php'>Admin: Add Blog</a>";
						echo "</div>";
					}
					//display the first four upcoming blogs based on the current date
					$sql = "SELECT blogsID, blogName, blogSummary FROM Blogs ORDER BY blogDate ASC LIMIT 2";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='blogText'>";
								echo "<h3>" . $row['blogName'] . "</h3>";
								echo "<p>" . $row['blogSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/blogInfo.php?blogsID=" . $row['blogsID'] . "'>More info</a></p>";
							echo "</div>";
						}
					}	
				?>
			</article>
			
            <article class="column2">
				<?php
					//display the first four upcoming blogs based on the current date
					$sql = "SELECT blogsID, blogName, blogSummary FROM Blogs ORDER BY blogDate ASC LIMIT 2, 2";
					$data = $db->query($sql);
					if ($data->num_rows > 0) {
						while($row = $data->fetch_assoc()) {
							echo "<div class='blogText'>";
								echo "<h3>" . $row['blogName'] . "</h3>";
								echo "<p>" . $row['blogSummary'] . "</p>";
								echo "<p><a href='http://byteguyz.org/blogInfo.php?blogsID=" . $row['blogsID'] . "'>More info</a></p>";
							echo "</div>";
						}
					}	
				?>
            </article>
        </section>
        <?php require 'includes/footer.inc'; ?>
	</body>
</html>
<?php
	session_start();
	require 'includes/eventsRegister.inc';
	require 'includes/validation.inc';
	require 'includes/connect.inc';
	$eventID = $_GET['eventID'];
	require 'includes/eventInformation.inc';
	
	if (!isset($_SESSION['isAdmin'])) {
		header('Location: http://byteguyz.org');
	}
	
	//if the admin submits an event, run through validation and submit to database
	if(isset($_POST['login'])) {
		$errMsg = '';
		$eventName = validate($_POST['eventName']);
		$eventDate = validate($_POST['eventDate']);
		$eventLocation = validate($_POST['eventLocation']);
		$eventCapacity = validate($_POST['eventCapacity']);
		$eventMemberCost = validate($_POST['eventMemberCost']);
		$eventInfo = validate($_POST['eventInfo']);
		$eventSummary = validate($_POST['eventSummary']);
		$privateEvent = $_POST['privateEvent'];
		if ($privateEvent) {
			$privateEvent = 1;
		} else {
			$privateEvent = 0;
		}		
		
		if($eventName == '') {
			$errMsg .= 'You must enter your event name<br>';
		}
		elseif (!preg_match("/^[a-zA-Z ]{5,40}$/", $eventName)) {
			$errMsg .= 'Your event name must contain only letters and be between 5 to 40 characters in length<br>';
		}		
		if($eventDate == '') {
			$errMsg .= 'You must enter your date<br>';
		}
		if($eventLocation == '') {
			$errMsg .= 'You must enter your location<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9, ]{5,50}/", $eventLocation)) {
			$errMsg .= 'Your location must contain only alphanumeric characters and be between 5 to 50 characters in length<br>';
		}
		if($eventCapacity == '') {
			$errMsg .= 'You must enter your capacity<br>';
		}
		elseif (!preg_match("/^[0-9]{2,5}$/", $eventCapacity)) {
			$errMsg .= 'Your event capacity must contain only numbers and be between two to five characters in length<br>';
		}
		if($eventMemberCost == '') {
			$errMsg .= 'You must enter your cost<br>';
		}
		elseif (!preg_match("/^[0-9.]*$/", $eventMemberCost)) {
			$errMsg .= 'Your event member cost must contain only numbers and a decimal<br>';
		}
		if($eventInfo == '') {
			$errMsg .= 'You must enter your information on the event<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{100,}/", $eventInfo)) {
			$errMsg .= 'Your event information must be greater than 100 characters, and only include common punctuation<br>';
		}
		if($eventSummary == '') {
			$errMsg .= 'You must enter your summary on the event<br>';
		}
		elseif (!preg_match("/[a-zA-Z0-9\',!;\?\$\^:\/`\’\|~&\" @#%\*\{}\(\)_\+\.=\-–]{100,150}/", $eventSummary)) {
			$errMsg .= 'Your summary must be between 100 to 150 characters, and only include common punctuation<br>';
		}
		//if the form input passes validation, submit to database
		if (!$errMsg) {
			//if the form was submitted by an admin, submit directly to the events table
			if (isset($_SESSION['isAdmin'])) {
				updateEvents($eventName, $eventDate, $eventLocation, $eventCapacity, $eventMemberCost, $eventInfo, $eventSummary, $eventID, $privateEvent, $imageHeader);	
				$statement = $db->prepare("SELECT usersID FROM JoinedEvents WHERE eventID = ?");
				$statement->bind_param('d', $eventID);	
				$statement->execute();
				$statement->store_result();
				$statement->bind_result($usersID);
				while ($statement->fetch()) {
					//select users name and email in preperation for the email
					$stmt = $db->prepare("SELECT username, email FROM Users WHERE usersID = ?");
					$stmt->bind_param('d', $usersID);	
					$stmt->execute();
					$stmt->store_result();
					$stmt->bind_result($username, $email);
					$stmt->fetch();
					//email the user a confirmation of the editing of the event
					$emailto = $email;
					$toname = $username;
					$emailfrom = 'mail.byteguyz.org';
					$fromname = 'Admin';
					$subject = 'Event has been edited';
					$messagebody = "Greetings $username,\n\nThe event, $eventName, has recently been updated, this is a notification to let you know that this has occured! The event in question is located at http://byteguyz.org/eventInfo.php?eventID=$eventID";
					$headers = 
						'Return-Path: ' . $emailfrom . "\r\n" . 
						'From: ' . $fromname . ' <' . $emailfrom . '>' . "\r\n" . 
						'X-Priority: 3' . "\r\n" . 
						'X-Mailer: PHP ' . phpversion() .  "\r\n" . 
						'Reply-To: ' . $fromname . ' <' . $emailfrom . '>' . "\r\n" .
						'MIME-Version: 1.0' . "\r\n" . 
						'Content-Transfer-Encoding: 8bit' . "\r\n" . 
						'Content-Type: text/plain; charset=UTF-8' . "\r\n";
					$params = '-f ' . $emailfrom;
					$test = mail($emailto, $subject, $messagebody, $headers, $params);
				}
				header('Location: http://byteguyz.org/eventInfo.php?eventID=' . $eventID . '');
			}
		}
	}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Community Event Management</title>
        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>        
        <script src="js/script.js"></script>
	</head>
	
	<body>
		<?php require 'includes/header.inc'; ?>
		<section id="text_columns">
            <article class="column1">
				<div class="formStyle">
					<h2>Edit an Event</h2>
					<form action="eventsEdit.php?eventID=<?php echo $eventID; ?>" method="POST" id="eventForm">
						<div class="requiredField">
							<input name="eventName" id="eventName" type="text" placeholder="Name" value="<?php echo $eventName;?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventDate" id="eventDate" type="date" placeholder="yyyy-mm-dd"  min="<?php echo date('Y-m-d'); ?>" value="<?php echo $eventDate;?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventLocation" id="eventLocation" type="text" placeholder="Location" value="<?php echo $eventLocation;?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventCapacity" id="eventCapacity" type="text" placeholder="Capacity" value="<?php echo $eventCapacity;?>" required/>
						</div>
						<div class="requiredField">
							<input name="eventMemberCost" id="eventMemberCost" type="text" placeholder="Cost" value="<?php echo $eventMemberCost;?>" required/>
						</div>
						<div class="requiredField">
							<textarea rows="4" cols="50" name="eventInfo" id="eventInfo" form="eventForm" placeholder="Event Information" required><?php echo $eventInfo;?></textarea>			
						</div>
						<div class="requiredField">
							<textarea rows="4" cols="50" name="eventSummary" id="eventSummary" form="eventForm" placeholder="Event Summary - between 100 to 150 characters"  required><?php echo $eventSummary;?></textarea>			
						</div>
						<div class="requiredField">
							<label for="privateEvent">Keep Event Hidden:</label>
							<input id="privateEvent" type="checkbox" name="privateEvent" value="true" <?php if ($eventHidden == 1) { echo "checked"; } ?>>
						</div>
						<input type="submit" name="login" value="Edit"/>
						<span id="errorField" class="errorField">
							<?php 
								echo $errMsg; 
							?>
						</span>
					</form>
				</div>
			</article>
		</section>
		<?php require 'includes/footer.inc'; ?>
	</body>
</html>